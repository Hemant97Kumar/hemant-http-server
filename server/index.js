import HTTP from 'http';
import path from 'path';
import fs from 'fs';
import { fileURLToPath } from 'url';
import { v4 } from 'uuid';

const htmlFilePath = './../public/index.html';
const jsonFilePath = './../jsonData/data.json';

const _dirname = path.dirname(fileURLToPath(import.meta.url));

const server = HTTP.createServer((request, response) => {
    const URL = request.url;
    if(URL === '/html'){
        fs.readFile(path.resolve(_dirname, htmlFilePath), 'utf8', (error, data) => {
            if (error) {
                console.log(error);
                response.writeHead(404, {'content-type': 'text/html'});
                response.write('<h1>File not found.</h1>');
                response.end();
            } else {
                response.writeHead(200, {'content-type': 'text/html'});
                response.write(data);
                response.end();
            }
        });
    } else if (URL === '/json') {
        fs.readFile(path.resolve(_dirname, jsonFilePath), 'utf8', (error, data) => {
            if (error) {
                console.log(error);
                response.writeHead(404, {'content-type': 'application/json'});
                response.write('<h1>File not found.</h1>');
                response.end();
            } else {
                response.writeHead(200, {'content-type': 'application/json'});
                response.write(data);
                response.end();
            }
        });
    } else if (URL === '/uuid') {
        response.writeHead(200, {'content-type': 'application/json'});
        const jsonData = {
            uuid: v4()
        };
        const jsonStringify = JSON.stringify(jsonData);
        response.write(jsonStringify);
        response.end();
    } else {
        const urlArr = URL.split('/');
        const type = urlArr[1];
        if (type === 'status') {
            const code = urlArr[2];
            const status = HTTP.STATUS_CODES;
            if(status.hasOwnProperty(code)){
                response.writeHead(parseInt(code), {'content-type': 'text/html'});
                response.write(`${code} ${status[code]}`);
                response.end();
            }else{
                response.writeHead(400, {'content-type': 'text/html'});
                response.write(`Enter valid status code`);
                response.end();
            }
        } else if (type === 'delay') {
            const seconds = urlArr[2];
            setTimeout(() => {
                response.writeHead(200, {'content-type': 'text/html'});
                response.write(`Response after ${seconds} seconds`);
                response.end();
            }, parseInt(seconds) * 1000);
        } else {
            response.writeHead(200, {'content-type': 'text/html'});
            response.write(`Random URL- ${URL}`);
            response.end();
        }
    }
});

server.listen(3000, () => console.log('Listening on port 3000.'));